import tensorflow as tf
import keras
import keras.backend as K
import keras.engine as KE 
import keras.models as KM
import keras.layers as KL

## target: redefine resnet, decrease memory occupy
class Net_test():
    def resnet_graph(self,input_image, architecture, stage5=False, train_bn=True):
        assert architecture in ["resnet50", "resnet101"]
        # Stage 1
        x = KL.ZeroPadding2D((3, 3))(input_image)
        print("resnet_graph, zeropadding2d", x.shape)
        x = KL.Conv2D(64, (7, 7), strides=(2, 2), name='res_conv1', use_bias=True)(x)
        x = KL.Activation('relu')(x)
        C1 = x = KL.MaxPooling2D((3, 3), strides=(2, 2), padding="same",name='res_c1')(x)
        ## (2,256,256,64)
        C2 = KL.Conv2D(256,(3,3),strides=(1,1),padding='same',name='res_c2')(C1)
        ## (2,256,256,256)
        C3 = KL.Conv2D(512,(3,3),strides=(2,2),padding='same',name='res_c3')(C2)
        ## (2,128,128,512)
        C4 = KL.Conv2D(1024,(3,3),strides=(2,2),padding='same',name='res_c4')(C3)
        ## (2,64,64,1024)
        if stage5:
            C5 = KL.Conv2D(2048,(3,3),strides=(2,2),padding='same',name='res_c5')(C4)
        else:
            C5 = None
        ## (2,32,32,2048)
        return [C1,C2,C3,C4,C5]

ptner_net_test = Net_test()